import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { increment, decrement, incrementByAmount, fetchImages } from "../redux/counter/counterSlice";
import { RootState, AppDispatch } from "../redux/store";

export default function Counter() {
    const { value, images } = useSelector((state: RootState) => state.counter);
    //const images = useSelector((state: RootState) => state.counter.images);
    const dispatch = useDispatch<AppDispatch>();

    useEffect(() => {
        dispatch(fetchImages());
    }, []);

    return (
        <div>
            <div>{value}</div>
            <div>
                <button onClick={() => dispatch(decrement())}>-</button>
                <button onClick={() => dispatch(increment())}>+</button>
                <button onClick={() => dispatch(incrementByAmount(5))}>+5</button>
            </div>
            {images.length !== 0 && <pre style={{ textAlign: "left" }}>{JSON.stringify(images, null, 4)}</pre>}
        </div>
    );
}
